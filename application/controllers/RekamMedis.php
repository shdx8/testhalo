<?php

class RekamMedis extends CI_Controller
{
	function __construct()
    {
        parent::__construct();
        chek_role();
        $this->load->model('Model_rekammedis');
        $this->load->model('Model_pet');
        $this->load->model('Model_vet');
    }
    function index()
    {
        $data['record'] = $this->Model_rekammedis->tampil_data()->result();
		$data['id_new'] = $this->Model_rekammedis->auto_id();
        $this->template->load('template/template', 'rekammedis/data', $data);
        $this->load->view('template/datatables');

    }
    function post()
    {
        if (isset($_POST["submit"])) {
           
                // proses barang
                $idRekamMedis = $this->input->post('idRekamMedis');
                $pet = $this->input->post('pet');
                $keluhan = $this->input->post('keluhan');
                $vet = $this->input->post('vet');
                $diagnosa = $this->input->post('diagnosa');
                $tglPeriksa = $this->input->post('tglPeriksa');
                $data = array(
                    'idRekamMedis' => $idRekamMedis,
                    'idPet' => $pet,
                    'keluhan' => $keluhan,
                    'nip' => $vet,
                    'diagnosa' => $diagnosa,
                    'tglPeriksa' => $tglPeriksa,
                );
                $this->Model_rekammedis->post($data);
                redirect('rekammedis');
            
        } else {
            $this->load->model("Model_pet");
            $this->load->model("Model_vet");
            $data['pet'] =  $this->Model_pet->tampilkan_data();
            $data['vet'] =  $this->Model_vet->tampilkan_data();
            $this->template->load("template/template", "rekammedis/add", $data);
        }
    }
	function edit()
	{
		if (isset($_POST['submit'])) {
			//proses owner
			$this->Model_rekammedis->edit();
			redirect('rekammedis');
		} else {
			$id = $this->uri->segment(3);
			$data['record'] = $this->Model_rekammedis->get_one($id)->row_array();
			$this->template->load('template/template','rekammedis/edit', $data);
		}
	}

	function hapus()
	{
		$td = $this->uri->segment(3);
		$this->Model_rekammedis->hapus($td);
		redirect('rekam_medis');
	}

	private function _generateId()
    {
        // RM2019080200001
        $char = "RM";
        $table = "rekam_medis";
        $field = "idRekamMedis";
        $today = date('Ymd');

        $prefix = $char . $today;

        $lastKode = $this->MainModel->getId($prefix, $table, $field);
        $noUrut = (int) substr($lastKode, -5, 5);
        $noUrut++;

        $newKode = $char . $today . sprintf('%05s', $noUrut);
        return $newKode;
    }


	public function readAPI() {
		$data = $this->Model_rekammedis->getAll();
		echo json_encode($data->result_array());;
	}

    function detail($rmId){
        $id = encode_php_tags($rmId);
        $whereId = ['idRekamMedis' => $id];
        $data['title']  = "Detail Data Rekam Medis";
        $data['detail'] = $this->MainModel->getRekamMedis($whereId);
        $data['obat']   = $this->MainModel->getObatRM($whereId)->result();

        // Rincian Biaya
        $data['biaya_dokter'] = $this->config->item('biaya_dokter');
        $total_obat = $this->MainModel->sumObat($whereId);
        $data['total_harga'] = $total_obat + $data['biaya_dokter'];

		$this->template->load('template/template','rekammedis/detail', $data);

    }
}
