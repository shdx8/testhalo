<?php

class Pet extends CI_Controller
{

	function __construct(){
        parent::__construct();
        chek_role();
        $this->load->model('Model_pet');
        $this->load->model('Model_owner');
    }

	function index()
	{
		$data['record'] = $this->Model_pet->tampilkan_data();
		$this->template->load('template/template', 'pet/data', $data);
		$this->load->view('template/datatables');

	}

	function post()
	{
		if (isset($_POST['submit'])) {
			//proses pet
			$this->Model_pet->post();
			redirect('pet');
		} else {
			$this->template->load('template/template', 'pet/add');
		}
	}

	
	function edit()
	{
		if (isset($_POST['submit'])) {
			//proses pet
			$this->Model_pet->edit();
			redirect('pet');
		} else {
			$id = $this->uri->segment(3);
			$data['record'] = $this->Model_pet->get_one($id)->row_array();
			$this->template->load('template/template','pet/edit', $data);
		}
	}

	function hapus()
	{
		$td = $this->uri->segment(3);
		$this->Model_pet->hapus($td);
		redirect('pet');
	}
}
