<?php

class Obat extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		chek_role();
		$this->load->model('Model_obat');
	}

	function index()
	{
		$data['record'] = $this->Model_obat->tampilkan_data();
		$this->template->load('template/template', 'Obat/data', $data);
		$this->load->view('template/datatables');

	}

	function post()
	{
		if (isset($_POST['submit'])) {
			//proses obat
			$this->Model_obat->post();
			redirect('obat');
		} else {
			$this->template->load('template/template', 'obat/add');
		}
	}
	function edit()
	{
		if (isset($_POST['submit'])) {
			//proses obat
			$this->Model_obat->edit();
			redirect('obat');
		} else {
			$id = $this->uri->segment(3);
			$data['record'] = $this->Model_obat->get_one($id)->row_array();
			$this->template->load('template/template','obat/edit', $data);
		}
	}

	function hapus()
	{
		$td = $this->uri->segment(3);
		$this->Model_obat->hapus($td);
		redirect('obat');
	}
}
