<?php

class Pet extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        chek_role();
        $this->load->model('Model_pet');
        $this->load->model('Model_owner');
    }
    
    function index()
    {
        $data['record'] = $this->Model_pet->tampil_data()->result();
        $this->template->load('template/template', 'pet/data', $data);
        $this->load->view('template/datatables');

    }
    function post()
    {
        if (isset($_POST["submit"])) {
           
                // proses barang
                $id         =   $this->input->post('idPet');
                $namaPet = $this->input->post('namaPet');
                $owner = $this->input->post('owner');
                $jenisKelamin = $this->input->post('jenisKelamin');
                $data = array(
                    'namaPet' => $namaPet,
                    'ownerId' => $owner,
                    'jenisKelamin' => $jenisKelamin,
                );
                $this->Model_pet->post($data, $id);
                redirect('pet');
            
        } else {
            $id = $this->uri->segment(3);
            $this->load->model("Model_owner");
            $data['owner'] =  $this->Model_owner->tampilkan_data();
            $this->template->load("template/template", "pet/add", $data);
        }
    }

    function edit()
    {
        if (isset($_POST["submit"])) {
           
                // proses barang
                $id         =   $this->input->post('idPet');
                $namaPet = $this->input->post('namaPet');
                $owner = $this->input->post('owner');
                $jenisKelamin = $this->input->post('jenisKelamin');
                $data = array(
                    'namaPet' => $namaPet,
                    'ownerId' => $owner,
                    'jenisKelamin' => $jenisKelamin,
                );
                $this->Model_pet->edit($data, $id);
                $this->session->set_flashdata('message', 'Data Barang berhasil dirubah!');
                redirect('pet');
            
        } else {
            $id = $this->uri->segment(3);
            $this->load->model("Model_owner");
            $data['record']  =  $this->Model_pet->get_one($id)->row_array();
            $data['owner'] =  $this->Model_owner->tampilkan_data();
            $this->template->load("template/template", "pet/edit", $data);
        }
    }

    function hapus()
    {
        $id = $this->uri->segment(3);
        $this->Model_pet->hapus($id);
        $this->session->set_flashdata('message', 'Data Pet berhasil dihapus!');
        redirect('pet');
    }

}
