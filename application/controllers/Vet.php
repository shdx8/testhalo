<?php

class Vet extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		chek_role();
		$this->load->model('Model_vet');
	}

	function index()
	{
		$data['record'] = $this->Model_vet->tampilkan_data();
		$this->template->load('template/template', 'Vet/data', $data);
		$this->load->view('template/datatables');

	}

	function post()
	{
		if (isset($_POST['submit'])) {
			//proses vet
			$this->Model_vet->post();
			redirect('vet');
		} else {
			$this->template->load('template/template', 'vet/add');
		}
	}
	function edit()
	{
		if (isset($_POST['submit'])) {
			//proses vet
			$this->Model_vet->edit();
			redirect('vet');
		} else {
			$id = $this->uri->segment(3);
			$data['record'] = $this->Model_vet->get_one($id)->row_array();
			$this->template->load('template/template','vet/edit', $data);
		}
	}

	function hapus()
	{
		$td = $this->uri->segment(3);
		$this->Model_vet->hapus($td);
		redirect('vet');
	}
}
