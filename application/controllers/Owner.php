<?php
class Owner extends CI_Controller{

	function __construct(){
		parent::__construct();
		chek_role();
		$this->load->model('Model_owner');
	}

	function index(){
		$data['record'] = $this->Model_owner->tampilkan_data();
		$this->template->load('template/template', 'Owner/data', $data);
		$this->load->view('template/datatables');

	}

	function post(){
		if (isset($_POST['submit'])) {
			//proses owner
			$this->Model_owner->post();
			redirect('owner');
		} else {
			$this->template->load('template/template', 'owner/add');
		}
	}
	function edit(){
		if (isset($_POST['submit'])) {
			//proses owner
			$this->Model_owner->edit();
			redirect('owner');
		} else {
			$id = $this->uri->segment(3);
			$data['record'] = $this->Model_owner->get_one($id)->row_array();
			$this->template->load('template/template','owner/edit', $data);
		}
	}

	function hapus(){
		$td = $this->uri->segment(3);
		$this->Model_owner->hapus($td);
		redirect('Owner');
	}
}
