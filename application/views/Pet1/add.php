<section class="content">
	<div class="row">
		<div class='col-xs-12'>
			<div class='box box-primary'>
				<div class='box-header  with-border'>
					<h3 class='box-title'>Tambah Data Pet</h3>
				</div>
				<div class="box-body">
					<?php echo form_open_multipart('owner/post', array('role' => "form", 'id' => "myForm", 'data-toggle' => "validator")); ?>
                    <?= form_open(); ?>
					<div class="form-group">
						<label for="namaPet" class="control-label">Nama Pet</label>
						<div class="input-group">
							<input type="text" class="form-control" name="namaPet" id="namaPet" data-error="Nama Pet harus diisi" placeholder="nama pet" value="<?= set_value('namaPet'); ?>" required />
							<span class="input-group-addon">
								<span class="fa fa-cube"></span>
							</span>
						</div>
						<div class="help-block with-errors"></div>
					</div>
					
					<div class="form-group">
						<label for="namaOwner" class="control-label">Nama Owner</label>
						<div class="input-group">
							<input type="text" class="form-control" name="namaOwner" id="namaOwner" data-error="Nama Owner harus diisi" placeholder="nama owner" value="<?= set_value('namaOwner'); ?>" required />
							<span class="input-group-addon">
								<span class="fa fa-cube"></span>
							</span>
						</div>
						<div class="help-block with-errors"></div>
					</div>
					
					<div class="form-group">
                    <label for="ownerId">Owner</label>
                    <select required name="ownerId" id="ownerId" class="form-control select2">
                        <option value="" selected disabled>Pilih Owner</option>
                        <?php foreach ($data['owner'] as $owner) : ?>
                            <option value="<?= $owner->ownerId ?>"><?= $owner->namaOwner ?></option>
                        <?php endforeach; ?>
                    </select>
                    <?= form_error('ownerId'); ?>
                </div>

				<div class="form-group">
						<label for="namaOwner" class="control-label">Pilih Owner</label>
						<div class="input-group">
							<select class="form-control" name="namaOwner">
							<option value="" selected disabled>Pilih Owner</option>
								<?php
								foreach ($owner as $o) {
									echo "<option value=' $o->idOwner'>$o->namaOwner</option>";
								}
								?>
							</select>
							<span class="input-group-addon">
								<span class="fa fa-list"></span>
							</span>
						</div>
						<div class="help-block with-errors"></div>
					</div>


					<div class="form-group">
                    <label>Jenis Kelamin</label>
                    <div class="row">
                        <div class="col">
                            <div class="custom-control custom-radio">
                                <input <?= set_radio('jenisKelamin', 'Jantan'); ?> value="Jantan" class="custom-control-input" type="radio" id="jantan" name="jenisKelamin">
                                <label for="jantan" class="custom-control-label">Jantan</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="custom-control custom-radio">
                                <input <?= set_radio('jenisKelamin', 'Betina'); ?> value="Betina" class="custom-control-input" type="radio" id="betina" name="jenisKelamin">
                                <label for="betina" class="custom-control-label">Betina</label>
                            </div>
                        </div>
                    </div>
                    <?= form_error('jenisKelamin'); ?>
                </div>
					
					<div class="box-footer">
						<button type="submit" name="submit" class="btn btn-primary ">Simpan</button>
						<a href="<?php echo base_url() ?>owner" class="btn btn-default ">Cancel</a>
					</div>
                    <?= form_close(); ?>
					</form>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div>
	</div>
</section>