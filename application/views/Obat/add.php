<section class="content">
	<div class="row">
		<div class='col-xs-12'>
			<div class='box box-primary'>
				<div class='box-header  with-border'>
					<h3 class='box-title'>Tambah Data Obat</h3>
				</div>
				<div class="box-body">
					<?php echo form_open_multipart('obat/post', array('role' => "form", 'id' => "myForm", 'data-toggle' => "validator")); ?>
                    <?= form_open(); ?>
					<div class="form-group">
						<label for="namaObat" class="control-label">Nama Obat</label>
						<div class="input-group">
							<input type="text" class="form-control" name="namaObat" id="namaObat" data-error="Nama Obat harus diisi" placeholder="nama obat" value="<?= set_value('namaObat'); ?>" required />
							<span class="input-group-addon">
								<span class="fa fa-cube"></span>
							</span>
						</div>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="harga" class="control-label">Harga</label>
						<div class="input-group">
							<input type="text" name="harga" id="harga" data-error="harga harus di isi" class="form-control" placeholder="harga" value="<?= set_value('harga'); ?>" required>
							<span class="input-group-addon">
								<span class="fas fa-home">
								</span>
							</span>
						</div>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="keterangan" class="control-label">Keterangan</label>
						<div class="input-group">
							<input type="text" name="keterangan" id="keterangan" data-error="keterangan harus di isi" class="form-control" placeholder="keterangan" value="<?= set_value('keterangan'); ?>" required>
							<span class="input-group-addon">
								<span class="fas fa-phone">
								</span>
							</span>
						</div>
						<div class="help-block with-errors"></div>
					</div>
					
					<div class="box-footer">
						<button type="submit" name="submit" class="btn btn-primary ">Simpan</button>
						<a href="<?php echo base_url() ?>obat" class="btn btn-default ">Cancel</a>
					</div>
                    <?= form_close(); ?>
					</form>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div>
	</div>
</section>