<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/app/css/style.css">
<?php if ($this->session->flashdata('message')) { ?>
<div class="col-lg-12 alerts">
	<div class="alert alert-dismissible alert-danger">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<h4> <i class="icon fa fa-ban"></i> Error</h4>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
</div>
<?php } else { } ?>

<section class="content">
	<div class="row">
		<div class='col-xs-12'>
			<div class='box box-primary'>
				<div class='box-header  with-border'>
					<h3 class='box-title'>Edit Data Vet</h3>
				</div>
				<div class="box-body">
					<?php echo form_open_multipart('Vet/edit', array('role' => "form", 'id' => "myForm", 'data-toggle' => "validator")); ?>
					<div class="form-group">
						<label for="nip" class="control-label">NIP</label>
						<div class="input-group">
							<input type="text" readonly class="form-control" name="nip" id="nip" value="<?php echo $record['nip'] ?>" data-error="NIP harus diisi" placeholder="nip" value="" required />
							<span class="input-group-addon">
								<span class="fa fa-cube"></span>
							</span>
						</div>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="namaVet" class="control-label">Nama Vet</label>
						<div class="input-group">
							<input type="text" class="form-control" name="namaVet" id="namaVet" value="<?php echo $record['namaVet'] ?>" data-error="Nama Vet harus diisi" placeholder="nama vet" value="" required />
							<span class="input-group-addon">
								<span class="fa fa-cube"></span>
							</span>
						</div>
						<div class="help-block with-errors"></div>
					</div>
				
					<div class="form-group">
						<label for="email" class="control-label">Email</label>
						<div class="input-group">
							<input type="text" class="form-control" name="email" id="email" value="<?php echo $record['email'] ?>" data-error="Email harus diisi" placeholder="email" value="" required />
							<span class="input-group-addon">
								<span class="fa fa-cube"></span>
							</span>
						</div>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="alamat" class="control-label">Alamat</label>
						<div class="input-group">
							<input type="text" class="form-control" name="alamat" id="alamat" value="<?php echo $record['alamat'] ?>" data-error="Alamat harus diisi" placeholder="alamat" value="" required />
							<span class="input-group-addon">
								<span class="fa fa-cube"></span>
							</span>
						</div>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="noTelp" class="control-label">noTelp Telepon</label>
						<div class="input-group">
							<input type="text" class="form-control" name="noTelp" id="noTelp" value="<?php echo $record['noTelp'] ?>" data-error="noTelp harus diisi" placeholder="noTelp" value="" required />
							<span class="input-group-addon">
								<span class="fa fa-cube"></span>
							</span>
						</div>
						<div class="help-block with-errors"></div>
					</div>
					
					
					<div class="box-footer">
						<input type="hidden" name="id" value="<?php echo $record['nip'] ?>">
						<button type="submit" name="submit" class="btn btn-primary ">Simpan</button>
						<a href="<?php echo base_url() ?>vet" class="btn btn-default ">Cancel</a>
					</div>
					</form>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div>
	</div>
</section>