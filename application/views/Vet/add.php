<section class="content">
	<div class="row">
		<div class='col-xs-12'>
			<div class='box box-primary'>
				<div class='box-header  with-border'>
					<h3 class='box-title'>Tambah Data Vet</h3>
				</div>
				<div class="box-body">
					<?php echo form_open_multipart('vet/post', array('role' => "form", 'id' => "myForm", 'data-toggle' => "validator")); ?>
                    <?= form_open(); ?>
					<div class="form-group">
						<label for="nip" class="control-label">NIP</label>
						<div class="input-group">
							<input type="text" class="form-control" name="nip" id="v" data-error="NIP harus diisi" placeholder="NIP" value="<?= set_value('nip'); ?>" required />
							<span class="input-group-addon">
								<span class="fa fa-cube"></span>
							</span>
						</div>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="namaVet" class="control-label">Nama Vet</label>
						<div class="input-group">
							<input type="text" class="form-control" name="namaVet" id="namaVet" data-error="Nama Vet harus diisi" placeholder="Nama Vet" value="<?= set_value('namaVet'); ?>" required />
							<span class="input-group-addon">
								<span class="fa fa-cube"></span>
							</span>
						</div>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="email" class="control-label">Email</label>
						<div class="input-group">
							<input type="text" name="email" id="email" data-error="email harus di isi" class="form-control" placeholder="Email" value="<?= set_value('email'); ?>" required>
							<span class="input-group-addon">
								<span class="fas fa-home">
								</span>
							</span>
						</div>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
					<div class="form-group">
						<label for="alamat" class="control-label">Alamat</label>
						<div class="input-group">
							<input type="text" name="alamat" id="alamat" data-error="alamat harus di isi" class="form-control" placeholder="Alamat" value="<?= set_value('alamat'); ?>" required>
							<span class="input-group-addon">
								<span class="fas fa-home">
								</span>
							</span>
						</div>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="noTelp" class="control-label">Nomor Telepon</label>
						<div class="input-group">
							<input type="text" name="noTelp" id="noTelp" data-error="NoTelp harus di isi" class="form-control" placeholder="noTelp" value="<?= set_value('noTelp'); ?>" required>
							<span class="input-group-addon">
								<span class="fas fa-phone">
								</span>
							</span>
						</div>
						<div class="help-block with-errors"></div>
					</div>
					
					<div class="box-footer">
						<button type="submit" name="submit" class="btn btn-primary ">Simpan</button>
						<a href="<?php echo base_url() ?>owner" class="btn btn-default ">Cancel</a>
					</div>
                    <?= form_close(); ?>
					</form>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div>
	</div>
</section>