<section class="content">
	<div class="row">
		<div class='col-xs-12'>
			<div class='box box-primary'>
				<div class='box-header  with-border'>
					<h3 class='box-title'>Tambah Data Rekam Medis</h3>
				</div>
				<div class="box-body">
					<?php echo form_open_multipart('rekammedis/post', array('role' => "form", 'id' => "myForm", 'data-toggle' => "validator")); ?>
                    <?= form_open(); ?>
					<!-- <div class="form-group">
						<label for="idRekamMedis" class="control-label">ID Rekam Medis</label>
						<div class="input-group">
                        <input readonly value="<?= set_value('idRekamMedis', $idRekamMedis); ?>" type="text" id="idRekamMedis" class="form-control" placeholder="ID Rekam Medis">
						</div>
						<div class="help-block with-errors"></div>
					</div> -->
                    <!-- <div class="form-group">
							<input type="text" name="idRekamMedis" class="form-control" value="<?= $id_new ?>" readonly hidden>
						</div>
					<div class="form-group"> -->
						<label for="pet" class="control-label">Pet</label>
						<div class="input-group">
							<select class="form-control" name="pet">
                            <option value="">Pilih Pet</option>
								<?php
								foreach ($pet as $p) {
									echo "<option value=' $p->idPet'>$p->namaPet</option>";
								}
								?>
							</select>
							<span class="input-group-addon">
								<span class="fa fa-list"></span>
							</span>
						</div>
						<div class="help-block with-errors"></div>
					<div class="form-group">
						<label for="keluhan">Keluhan</label>
							<textarea required name="keluhan" id="keluhan" rows="2" class="form-control" placeholder="Keluhan"><?= set_value('keluhan'); ?></textarea>
						<?= form_error('keluhan'); ?>
                	</div>
					<div class="form-group">
						<label for="vet" class="control-label">Vet</label>
						<div class="input-group">
							<select class="form-control" name="vet">
                                <option value="">Pilih Vet</option>
								<?php
								foreach ($vet as $v) {
									echo "<option value=' $v->nip'>$v->namaVet</option>";
								}
								?>
							</select>
							<span class="input-group-addon">
								<span class="fa fa-list"></span>
							</span>
						</div>
						<div class="help-block with-errors"></div>
					</div>
                <div class="form-group">
                    <label for="idObat">Obat</label>
                    <select required multiple name="idObat[]" id="idObat" class="form-control select2">
                        <?php foreach ($data['obat'] as $obat) : ?>
                            <option value="<?= $obat->idObat ?>"><?= $obat->namaObat; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <?= form_error('idObat'); ?>
                </div>
                <div class="form-group">
                    <label for="tglPeriksa">Tanggal Periksa</label>
                    <input required value="<?= set_value('tglPeriksa', date('Y-m-d')); ?>" type="date" name="tglPeriksa" id="tglPeriksa" class="form-control gijgo" placeholder="Tanggal Periksa">
                    <?= form_error('tglPeriksa'); ?>
                </div>
					<div class="box-footer">
						<button type="submit" name="submit" class="btn btn-primary ">Simpan</button>
						<a href="<?php echo base_url() ?>rekammedis" class="btn btn-default ">Cancel</a>
					</div>
                    <?= form_close(); ?>
					</form>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div>
	</div>
</section>