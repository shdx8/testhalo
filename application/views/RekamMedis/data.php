<style type="text/css">
	table,
	th,
	tr,
	td {
		text-align: center;
	}

	.swal2-popup {
		font-family: inherit;
		font-size: 1.2rem;
	}
</style>
<section class="content">
	<div class="row">
	<div class="col-md-12">
			<div class="box box-info">
				<div class='box-header  with-border'>
					<h3 class='box-title'>Rekam Medis</h3>
					<div class="pull-right">
						<?php
						echo anchor('rekammedis/post', 'Tambah Data', array('class' => 'btn btn-success'));
						?>
					</div>
				</div>
				<div class="box-body table-responsive">
					<table id="myTable" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>No</th>
								<th>Tanggal Periksa</th>
                                <th>Pet</th>
                                <th>Keluhan</th>
                                <th>Vet</th>
                                <th>Diagnosa</th>
                                <th style="width: 20%">Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$no = 1;
							foreach ($record as $a) {  ?>
								<tr>
                                    <td><?= $no++ ?>.</td>
                                    <td><?= indo_date($a->tglPeriksa); ?></td>
                                    <td><?= $a->namaPet ?></td>
                                    <td><?= $a->keluhan ?></td>
                                    <td><?= $a->namaVet ?></td>
                                    <td><?= $a->diagnosa ?></td>
									<td><?php
										echo anchor(site_url('rekammedis/detail/' . $a->idRekamMedis), '<i class="fa fa-eye"></i>&nbsp;&nbsp;', array('class' => 'btn btn-sm btn-success'));
										echo '&nbsp';
										echo anchor(site_url('rekammedis/edit/' . $a->idRekamMedis), '<i class="fa fa-pencil-square-o fa-lg"></i>&nbsp;&nbsp;', array('class' => 'btn btn-sm btn-warning'));
										echo '&nbsp';
										echo anchor(site_url('rekammedis/hapus/' . $a->idRekamMedis), '<i class="fa fa-trash fa-lg"></i>&nbsp;&nbsp;', 'class="btn btn-sm btn-danger "');
										?>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

</section>

<script src="<?php echo base_url() ?>assets/app/js/alert.js"></script>
<script>
	$(document).ready(function() {
		$('#myTable').DataTable();
	});
</script>