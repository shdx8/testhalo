<div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"><i class="fas fa-notes-medical"></i> Detail Data Rekam Medis</h1>
                </div>
				<!-- /.col -->
                
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">

<div class="row">
    <div class="col-md-12">
        <div class="card card-outline">
  
            <!-- /.card-header -->
            <div class="card-body">
                <div class="row mb-6">
                    <div class="col-sm p-6">
                        <table class="w-100 table-sm table-hover">
                            <tr>
                                <th width="150">ID Rekam Medis</th>
                                <td><?= $detail->idRekamMedis; ?></td>
                            </tr>
                            <tr>
                                <th width="150">Pet</th>
                                <td><?= $detail->namaPet; ?></td>
                            </tr>
                           <tr>
                                <th width="150">Keluhan Pet</th>
                                <td><?= $detail->keluhan; ?></td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm p-6">
                        <table class="w-100 table-sm table-hover">
                            <tr>
                                <th width="150">Tanggal Periksa</th>
                                <td><?= indo_date($detail->tglPeriksa); ?></td>
                            </tr>
                            <tr>
                                <th width="150">Dokter</th>
                                <td><?= $detail->namaDokter; ?></td>
                            </tr>
                            <tr>
                                <th width="150">Petugas</th>
                                <td><?= $detail->fullName; ?></td>
                            </tr>
							<tr>
                                <th width="150">Diagnosa</th>
                                <td><?= $detail->diagnosa; ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
  
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <div class="col-md-12">
        <div class="card card-outline">
            <div class="card-header bg-danger">
                <h3 class="card-title"> Rincian Biaya</h3>
            </div>
            <div class="card-body">
                <table class="w-100 table-sm table-hover">
                    <tr>
                        <th>Biaya Dokter</th>
                        <td class="text-right">
                            Rp. <?= number_format($biaya_dokter, 2, ',', '.'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th colspan="2">Biaya Obat</th>
                    </tr>
                    <?php foreach ($obat as $o) : ?>
                        <tr>
                            <td>+ <?= $o->namaObat ?></td>
                            <td class="text-right">
                                Rp. <?= number_format($o->harga, 2, ',', '.'); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <tr>
                        <td colspan="2">
                            <hr>
                        </td>
                    </tr>
                    <tr>
                        <th>Total Harga</th>
                        <td class="text-right">
                            Rp. <?= number_format($total_harga, 2, ',', '.'); ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>