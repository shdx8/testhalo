<section class="content">
	<div class="row">
		<div class='col-xs-12'>
			<div class='box box-primary'>
				<div class='box-header  with-border'>
					<h3 class='box-title'>Edit Data Rekam Medis</h3>
				</div>
				<div class="box-body">
					<?php echo form_open_multipart('rekammedis/post', array('role' => "form", 'id' => "myForm", 'data-toggle' => "validator")); ?>
                    <?= form_open(); ?>
					<div class="form-group">
						<label for="idRekamMedis" class="control-label">ID RekamMedis</label>
						<div class="input-group">
							<input type="text" class="form-control" readonly name="idRekamMedis" id="idRekamMedis" value="<?php echo $record['idRekamMedis'] ?>" placeholder="idRekamMedis" value="" required />
							<span class="input-group-addon">
								<span class="fa fa-cube"></span>
							</span>
						</div>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="petId" class="control-label">Pet</label>
						<select required name="petId" id="petId" class="form-control select2">
                        <option value="" selected disabled>Pilih Pet</option>
                        <?php foreach ($data['pet'] as $pet) : ?>
                            <option value="<?= $pet->idPet ?>"><?= $pet->namaPet ?></option>
                        <?php endforeach; ?>
                    </select>
                    <?= form_error('petId'); ?>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="keluhan" class="control-label">Keluhan</label>
						<div class="input-group">
							<input type="text" class="form-control" name="keluhan" id="keluhan" value="<?php echo $record['keluhan'] ?>" placeholder="keluhan" value="" required />
							<span class="input-group-addon">
								<span class="fa fa-cube"></span>
							</span>
						</div>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
                    <label for="dokterNip">Dokter</label>
                    <select required name="dokterNip" id="dokterNip" class="form-control select2">
                        <option value="" selected disabled>Pilih Dokter</option>
                        <?php foreach ($data['dokter'] as $dokter) : ?>
                            <option value="<?= $dokter->nip ?>"><?= $dokter->nip; ?> | <?= $dokter->namaDokter; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <?= form_error('dokterNip'); ?>
                </div>
                <div class="form-group">
                    <label for="diagnosa">Diagnosa</label>
                    <textarea required name="diagnosa" id="diagnosa" value="<?php echo $record['diagnosa'] ?>" rows="2" class="form-control" placeholder="Diagnosa"><?= set_value('diagnosa', $rekam_medis->diagnosa); ?></textarea>
                    <?= form_error('diagnosa'); ?>
                </div>

                <div class="form-group">
						<label for="diagnosa" class="control-label">Diagnosa</label>
						<div class="input-group">
							<input type="text" class="form-control" name="diagnosa" id="diagnosa" value="<?php echo $record['diagnosa'] ?>" placeholder="diagnosa" value="" required />
							<span class="input-group-addon">
								<span class="fa fa-cube"></span>
							</span>
						</div>
						<div class="help-block with-errors"></div>
					</div>

                <div class="form-group">
                    <label for="idObat">Obat</label>
                    <select required multiple name="idObat[]" id="idObat" class="form-control select2">
                        <?php foreach ($data['obat'] as $obat) : ?>
                            <option value="<?= $obat->idObat ?>"><?= $obat->namaObat; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <?= form_error('idObat'); ?>
                </div>
                <div class="form-group">
                    <label for="tglPeriksa">Tanggal Periksa</label>
                    <input required value="<?= set_value('tglPeriksa', date('Y-m-d')); ?>" type="date" name="tglPeriksa" id="tglPeriksa" value="<?php echo $record['tglPeriksa'] ?>" class="form-control gijgo" placeholder="Tanggal Periksa">
                    <?= form_error('tglPeriksa'); ?>
                </div>
					<div class="box-footer">
						<button type="submit" name="submit" class="btn btn-primary ">Simpan</button>
						<a href="<?php echo base_url() ?>rekammedis" class="btn btn-default ">Cancel</a>
					</div>
                    <?= form_close(); ?>
					</form>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div>
	</div>
</section>