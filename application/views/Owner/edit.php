<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/app/css/style.css">
<?php if ($this->session->flashdata('message')) { ?>
<div class="col-lg-12 alerts">
	<div class="alert alert-dismissible alert-danger">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<h4> <i class="icon fa fa-ban"></i> Error</h4>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
</div>
<?php } else { } ?>

<section class="content">
	<div class="row">
		<div class='col-xs-12'>
			<div class='box box-primary'>
				<div class='box-header  with-border'>
					<h3 class='box-title'>Edit Data Owner</h3>
				</div>
				<div class="box-body">
					<?php echo form_open_multipart('Owner/edit', array('role' => "form", 'id' => "myForm", 'data-toggle' => "validator")); ?>
					<div class="form-group">
						<label for="namaOwner" class="control-label">Nama Owner</label>
						<div class="input-group">
							<input type="text" class="form-control" name="namaOwner" id="namaOwner" value="<?php echo $record['namaOwner'] ?>" data-error="Nama Owner harus diisi" placeholder="nama owner" value="" required />
							<span class="input-group-addon">
								<span class="fa fa-cube"></span>
							</span>
						</div>
						<div class="help-block with-errors"></div>
					</div>
				
					<div class="form-group">
						<label for="alamat" class="control-label">alamat</label>
						<div class="input-group">
							<input type="text" class="form-control" name="alamat" id="alamat" value="<?php echo $record['alamat'] ?>" data-error="alamat harus diisi" placeholder="alamat" value="" required />
							<span class="input-group-addon">
								<span class="fa fa-cube"></span>
							</span>
						</div>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="nomor" class="control-label">Nomor Telepon</label>
						<div class="input-group">
							<input type="text" class="form-control" name="nomor" id="nomor" value="<?php echo $record['nomor'] ?>" data-error="Nomor harus diisi" placeholder="nomor" value="" required />
							<span class="input-group-addon">
								<span class="fa fa-cube"></span>
							</span>
						</div>
						<div class="help-block with-errors"></div>
					</div>
					
					
					<div class="box-footer">
						<input type="hidden" name="id" value="<?php echo $record['idOwner'] ?>">
						<button type="submit" name="submit" class="btn btn-primary ">Simpan</button>
						<a href="<?php echo base_url() ?>owner" class="btn btn-default ">Cancel</a>
					</div>
					</form>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div>
	</div>
</section>