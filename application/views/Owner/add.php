<section class="content">
	<div class="row">
		<div class='col-xs-12'>
			<div class='box box-primary'>
				<div class='box-header  with-border'>
					<h3 class='box-title'>Tambah Data Owner</h3>
				</div>
				<div class="box-body">
					<?php echo form_open_multipart('owner/post', array('role' => "form", 'id' => "myForm", 'data-toggle' => "validator")); ?>
                    <?= form_open(); ?>
					<div class="form-group">
						<label for="namaOwner" class="control-label">Nama Owner</label>
						<div class="input-group">
							<input type="text" class="form-control" name="namaOwner" id="namaOwner" data-error="Nama Owner harus diisi" placeholder="nama owner" value="<?= set_value('namaOwner'); ?>" required />
							<span class="input-group-addon">
								<span class="fa fa-cube"></span>
							</span>
						</div>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="alamat" class="control-label">Alamat</label>
						<div class="input-group">
							<input type="text" name="alamat" id="alamat" data-error="alamat harus di isi" class="form-control" placeholder="Alamat" value="<?= set_value('alamat'); ?>" required>
							<span class="input-group-addon">
								<span class="fas fa-home">
								</span>
							</span>
						</div>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="nomor" class="control-label">Nomor Telepon</label>
						<div class="input-group">
							<input type="text" name="nomor" id="nomor" data-error="nomor harus di isi" class="form-control" placeholder="Nomor" value="<?= set_value('nomor'); ?>" required>
							<span class="input-group-addon">
								<span class="fas fa-phone">
								</span>
							</span>
						</div>
						<div class="help-block with-errors"></div>
					</div>
					
					<div class="box-footer">
						<button type="submit" name="submit" class="btn btn-primary ">Simpan</button>
						<a href="<?php echo base_url() ?>owner" class="btn btn-default ">Cancel</a>
					</div>
                    <?= form_close(); ?>
					</form>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div>
	</div>
</section>