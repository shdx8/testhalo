<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Point Of Sales | Log in</title>
  <link rel="icon" href="<?=base_url()?>assets/dist/img/halopet_logo.png" type="image/png">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/iCheck/square/blue.css">

  <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/animation/style.css">
</head>

<body onload="openEyes()">

  <div class="container text-center" id="loginContainer">
 <img src="<?php echo base_url(); ?>assets/dist/img/halopet_logo.png"/>
    <!--svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-120 100 1920 600" style="enable-background:new 0 0 1920 600;" xml:space="preserve"-->
      

    <div style="width: 100%">
 
      <div style="margin: 40px 40px;">
        <?php
        echo form_open('auth/login', array('style' => 'text-align:center;'));
        ?>
        <?php $error = $this->session->flashdata('message_name');
        ?>
        <p align="center" style="color:red;"><?php echo $error; ?></p>
        <input onfocus="openEyes();" onblur="movePupilsToCenter();" name="username" id="username" type="text" placeholder="Username">
      </div>
    </div>
    <div style="margin: 40px 40px;">
      <div style="text-align: center">
        <input onfocus="closeEyes();" onblur="openEyes();" name="password" id="password" type="password" placeholder="Password">
      </div>
    </div>
    <div style="margin: 40px 40px;">
      <div style="text-align: center">
        <button type="submit" name="submit">Login</button>
      </div>
      </form>
    </div>
  </div>
  </div>
  <script rel="script" src="<?php echo base_url() ?>assets/plugins/animation/javascript.js"></script>
</body>

</html>