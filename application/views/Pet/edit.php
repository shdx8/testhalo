<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/app/css/style.css">
<?php if ($this->session->flashdata('message')) { ?>
<div class="col-lg-12 alerts">
	<div class="alert alert-dismissible alert-danger">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<h4> <i class="icon fa fa-ban"></i> Error</h4>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
</div>
<?php } else { } ?>
<section class="content">
	<div class="row">
		<div class='col-xs-12'>
			<div class='box box-primary'>
				<div class='box-header  with-border'>
					<h3 class='box-title'>Edit Data Pet</h3>
				</div>
				<div class="box-body">
					<?php echo form_open_multipart('pet/post', array('role' => "form", 'id' => "myForm", 'data-toggle' => "validator")); ?>
					<div class="form-group">
						<label for="namaPet" class="control-label">Nama Pet</label>
						<div class="input-group">
							<input type="text" class="form-control" name="namaPet" value="<?php echo $record['namaPet'] ?>" id="namaPet" data-error="Nama Pet harus diisi" placeholder="nama pet" value="<?= set_value('namaPet'); ?>" required />
							<span class="input-group-addon">
								<span class="fa fa-cube"></span>
							</span>
						</div>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<label for="owner" class="control-label">Nama Owner</label>
						<div class="input-group">
							<select class="form-control" name="owner">
							<option name="" id="">Pilih Owner</option>
								<?php
								foreach ($owner as $o) {
									if ($record['ownerId'] == $o->idOwner) {
										echo "<option value='$o->idOwner' selected='selected'>$o->namaOwner</option>";
									} else {
										echo "<option value=' $o->idOwner'>$o->namaOwner</option>";
									}
								}
								?>
							</select>
							<span class="input-group-addon">
								<span class="fa fa-list"></span>
							</span>
						</div>
						<div class="help-block with-errors"></div>
					</div>

					

					<div class="form-group">
                    <label>Jenis Kelamin</label>
                    <div class="row">
                        <div class="col">
                            <div class="custom-control custom-radio">
                                <input <?= set_radio('jenisKelamin', 'Jantan');?> class="custom-control-input" type="radio" id="jantan" name="jenisKelamin">
                                <label for="jantan" class="custom-control-label">Jantan</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="custom-control custom-radio">
                                <input <?= set_radio('jenisKelamin', 'Betina');?> class="custom-control-input" type="radio" id="betina" name="jenisKelamin">
                                <label for="betina" class="custom-control-label">Betina</label>
                            </div>
                        </div>
                    </div>
                    <?= form_error('jenisKelamin'); ?>
                </div>
					
					<div class="box-footer">
						<input type="hidden" name="idPet" value="<?php echo $record['idPet'] ?>">
						<button type="submit" name="submit" class="btn btn-primary ">Simpan</button>
						<a href="<?php echo base_url() ?>pet" class="btn btn-default ">Cancel</a>
					</div>
					</form>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div>
	</div>
</section>