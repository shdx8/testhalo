<?php
class Model_pet extends CI_Model{

	function tampil_data()
	{
		return
			$this->db->join('owner', 'owner.idOwner = pet.ownerId', 'left')
			->distinct()
			->get('pet');
	}

	function tampilkan_data() {

		return 
		$this->db->get('pet')->result(); 
	}

	function post($data)
	{
		$this->db->insert('pet', $data);
	}

	function get_one($id)
	{
		$param = array('idPet' => $id);
		return $this->db->get_where('pet', $param);
	}

	function edit($data, $id)
	{
		$this->db->where('idPet', $id);
		$this->db->update('pet', $data);
	}

	function hapus($id)
	{
		$this->db->where('idPet', $id);
		$this->db->delete('pet');
	}

	
}
