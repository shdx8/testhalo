<?php
class Model_vet extends CI_Model {

	function tampilkan_data() {

		return 
		$this->db->get('vet')->result(); 
	}
		function post()
		{
			$data=array
			(
				'nip'=> $this->input->post('nip'),
				'namaVet'=> $this->input->post('namaVet'),
				'email'=> $this->input->post('email'),
				'alamat'=> $this->input->post('alamat'),
				'noTelp'=> $this->input->post('noTelp')
			);
			$this->db->insert('vet', $data);
		}

		function edit()
		{
			$data=array
			(
				'namaVet'=> $this->input->post('namaVet'),
				'email'=> $this->input->post('email'),
				'alamat'=> $this->input->post('alamat'),
				'noTelp'=> $this->input->post('noTelp')
			);
			$this->db->where('nip', $this->input->post('id'));
			$this->db->update('vet',$data);
		}
		function get_one($id)
		{
			$param = array('nip'=>$id);
			return $this->db->get_where('vet',$param);
		}

		function hapus($id)
		{
			$this->db->where('nip', $id);
			$this->db->delete('vet');
		}

}