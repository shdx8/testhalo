<?php
class Model_rekammedis extends CI_Model {

	function tampil_data()
	{
		return
			$this->db->join('pet','pet.idPet = rekam_medis.petId','LEFT')
			->join('vet','vet.nip = rekam_medis.vetNip','LEFT')
			->distinct()
			->get('rekam_medis');
	}

	private function _generateId()
    {
        // RM2019080200001
        $char = "RM";
        $table = "rekam_medis";
        $field = "idRekamMedis";
        $today = date('Ymd');

        $prefix = $char . $today;

        $lastKode = $this->MainModel->getId($prefix, $table, $field);
        $noUrut = (int) substr($lastKode, -5, 5);
        $noUrut++;

        $newKode = $char . $today . sprintf('%05s', $noUrut);
        return $newKode;
    }

	function auto_id(){
		$query = $this->db->query("SELECT max(idRekamMedis) as id_new FROM rekam_medis");
		$data =$query->row_array();
		$idRekamMedis = $data['id_new'];

		// mengambil angka dari kode barang terbesar, menggunakan fungsi substr
		// dan diubah ke integer dengan (int)
		$urutan = (int) substr($idRekamMedis, 3, 3);

		// bilangan yang diambil ini ditambah 1 untuk menentukan nomor urut berikutnya
		$urutan++;

		// membentuk kode barang baru
		// perintah sprintf("%03s", $urutan); berguna untuk membuat string menjadi 3 karakter
		// misalnya perintah sprintf("%03s", 15); maka akan menghasilkan '015'
		// angka yang diambil tadi digabungkan dengan kode huruf yang kita inginkan, misalnya BRG 
		$huruf = "RM00000";
		$idRekamMedis = $huruf . sprintf("%03s", $urutan);
		return $idRekamMedis;
	}


	function post($data)
	{
		$this->db->insert('rekam_medis', $data);
	}

		// function post()
		// {
		// 	$data=array
		// 	(
		// 		'namaOwner'=> $this->input->post('namaOwner'),
		// 		'alamat'=> $this->input->post('alamat'),
		// 		'nomor'=> $this->input->post('nomor')
		// 	);
		// 	$this->db->insert('rekam_medis', $data);
		// }

		function edit()
		{
			$this->db->select('*');
		$this->db->from('rekam_medis');
      	$this->db->join('pet','pet.idPet = rekam_medis.petId','LEFT');      
      	$this->db->join('vet','vet.nip = rekam_medis.vetNip','LEFT');
      	$query = $this->db->get();
      	return $query->result();

		  
		  $data=array
		  (
			  'idRekamMedis'=> $this->input->post('idRekamMedis'),
			  'petId'=> $this->input->post('petId'),
			  'diagnosa'=> $this->input->post('diagnosa'),
			  'keluhan'=> $this->input->post('keluhan'),
			  'tglPeriksa'=> $this->input->post('tglPeriksa')
		  );
			$this->db->where('idRekamMedis', $this->input->post('id'));
			$this->db->update('rekam_medis',$data);
		}

		function get_one($id)
		{
			$param = array('idRekamMedis'=>$id);
			return $this->db->get_where('rekam_medis',$param);
		}

		function hapus($id)
		{
			$this->db->where('idRekamMedis', $id);
			$this->db->delete('rekam_medis');
		}

		function getAll(){//function getAll
			$this->db->select('*');//select semua data
			$this->db->from('rekam_medis');
			$this->db->join('pet','pet.idPet = rekam_medis.petId','LEFT');      
  	    	$this->db->join('vet','vet.nip = rekam_medis.dokterNip','LEFT');//dari table user
			$query = $this->db->get();
			return $query;//lakukan query db
		}

}

        