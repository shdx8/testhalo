<?php
class Model_pet extends CI_Model {

	function tampilkan_data() {

        $this->db->select('*');
		$this->db->from('pet');
      	$this->db->join('owner','owner.idOwner = pet.ownerId','LEFT');      
      	$query = $this->db->get();
      	return $query->result();
	}

	function post()
    {
        
                // proses barang
                $namaPet = $this->input->post('namaPet');
                $namaOwner = $this->input->post('namaOwner');
                $jenisKelamin = $this->input->post('jenisKelamin');
                $data = array(
                    'namaPet' => $namaPet,
                    'ownerId' => $namaOwner,
                    'jenisKelamin' => $jenisKelamin,
                );
                $this->db->insert('pet', $data);
				$this->load->model("Model_owner");
				$data['owner'] =  $this->Model_owner->tampilOwner();
                $this->session->set_flashdata('message', 'Data Barang berhasil ditambahkan!');
                redirect('pet');
            }
    

	
		function post1($where = null){
		$this->db->select('*');
		$this->db->from('pet');
      	$this->db->join('owner','owner.idOwner = pet.ownerId','LEFT');      
      	$query = $this->db->get();
      	return $query->result();

			$data=array
			(
				'namaPet'=> $this->input->post('namaPet'),
				'alamat'=> $this->input->post('alamat'),
				'nomor'=> $this->input->post('nomor')
			);
			$this->db->insert('pet', $data);
		}

		public function get($table)
    {
        return $this->db->get($table)->result();
    }

		function edit(){
			

			$data['owner'] = $this->Model_owner->getOwner();
			$data=array
			(
				'namaPet'=> $this->input->post('namaPet'),
				'namaOwner'=> $this->input->post('namaOwner'),
				'jenisKelamin'=> $this->input->post('jenisKelamin')
			);
			$this->db->where('idPet', $this->input->post('id'));
			$this->db->update('pet',$data);
		}

		function get_one($id)
		{
			$param = array('idPet'=>$id);
			return $this->db->get_where('pet',$param);
		}

		function hapus($id)
		{
			$this->db->where('idPet', $id);
			$this->db->delete('pet');
		}

		function edit_data($where,$table) {
			$this->db->join('owner', 'owner.idOwner = pet.ownerId');
			return $this->db->get_where($table, $where);
		}

}