<?php
class Model_obat extends CI_Model {

	function tampilkan_data() {

		return 
		$this->db->get('obat')->result(); 
	}
		function post()
		{
			$data=array
			(
				'namaObat'=> $this->input->post('namaObat'),
				'harga'=> $this->input->post('harga'),
				'keterangan'=> $this->input->post('keterangan')
			);
			$this->db->insert('obat', $data);
		}

		function edit()
		{
			$data=array
			(
				'namaObat'=> $this->input->post('namaObat'),
				'harga'=> $this->input->post('harga'),
				'keterangan'=> $this->input->post('keterangan')
			);
			$this->db->where('idObat', $this->input->post('id'));
			$this->db->update('obat',$data);
		}

		function get_one($id)
		{
			$param = array('idObat'=>$id);
			return $this->db->get_where('obat',$param);
		}

		function hapus($id)
		{
			$this->db->where('idObat', $id);
			$this->db->delete('obat');
		}

}