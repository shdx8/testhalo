<?php
class Model_owner extends CI_Model {

	function tampilkan_data() {

		return 
		$this->db->get('owner')->result(); 
	}
		function post(){
			$data=array
			(
				'namaOwner'=> $this->input->post('namaOwner'),
				'alamat'=> $this->input->post('alamat'),
				'nomor'=> $this->input->post('nomor')
			);
			$this->db->insert('owner', $data);
		}

		function edit()
		{
			$data=array
			(
				'namaOwner'=> $this->input->post('namaOwner'),
				'alamat'=> $this->input->post('alamat'),
				'nomor'=> $this->input->post('nomor')
			);
			$this->db->where('idOwner', $this->input->post('id'));
			$this->db->update('owner',$data);
		}

		function get_one($id)
		{
			$param = array('idOwner'=>$id);
			return $this->db->get_where('owner',$param);
		}

		function hapus($id)
		{
			$this->db->where('idOwner', $id);
			$this->db->delete('owner');
		}

		function getOwner(){
			$this->db->select('*');
			return $this->db->get('data_owner')->result();
		}
		function tampilOwner(){
		return
			$this->db->select('idOwner, namaOwner')
			->from('owner')
			->get();
	}

	function getAll(){//function getAll
		$this->db->select('*');//select semua data
		$this->db->from('owner');//dari table user
		$query = $this->db->get();
		return $query;//lakukan query db
	}
}